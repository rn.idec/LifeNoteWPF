﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifenoteData.Entity
{
    public partial class LifeNoteSQLdataEntities
    {
        private static LifeNoteSQLdataEntities _ctx;

        public static LifeNoteSQLdataEntities GetEntityContext()
        {
            if (_ctx == null)
                _ctx = new LifeNoteSQLdataEntities();
            return _ctx;
        }
    }
}
