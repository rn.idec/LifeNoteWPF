﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifenoteData.Entity
{
    public partial class Alarm
    {
        public Alarm(Alarm a)
        {
            IdAlarm = a.IdAlarm;
            TimeAlarm = a.TimeAlarm;

            Works = a.Works;
            Notes = a.Notes;
        }
        public static Alarm GetNewAlarm(TimeSpan time)
        {
                Alarm a = new Alarm();
                a.IdAlarm = -1;
                a.TimeAlarm = time;
                return a;
        }
        private bool _isnew = false;

        public bool IsNewAlarm
        {
            get { return _isnew; }
            set { _isnew = value; }
        }
        public override string ToString()
        {
            return TimeAlarm.ToString("hh:mm");
        }
    }
}
