﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifenoteData.Entity
{
    public partial class Work
    {
        //VARIABLES
        private TimeSpan _heuresEffective;
        private TimeSpan _totHeures;
        private TimeSpan _soir = new TimeSpan(20, 0, 0);
        private TimeSpan _untilMidnight = new TimeSpan(0, 0, 1);
        private TimeSpan _newMidnight = new TimeSpan(23, 59, 59);
        private bool _isweekend = false;
        private bool _isnew = false;
        //PROPRIETES
        public bool IsWeekend
        {
            get
            {
                return _isweekend;
            }
            set
            {
                _isweekend = value;

            }
        }
        public TimeSpan TotHeures
        {
            get
            {
                if (PauseStart != null && PauseEnd != null)
                {
                    if (TimeEnd == TimeSpan.Zero && PauseStart.HasValue)
                        _totHeures = (PauseStart.Value).Subtract(TimeStart) + _newMidnight.Subtract(PauseEnd.Value) + _untilMidnight;
                    else if (TimeEnd == TimeSpan.Zero && PauseStart == null)
                        _totHeures = _newMidnight.Subtract(TimeStart) + _untilMidnight;
                    else if (PauseStart.HasValue)
                        _totHeures = (PauseStart.Value).Subtract(TimeStart) + TimeEnd.Subtract(PauseEnd.Value);
                }
                else
                    _totHeures = TimeEnd.Subtract(TimeStart);

                return _totHeures;
            }
        }
        public TimeSpan HeuresEffective
        {
            get
            {
                if (TimeEnd > _soir)
                {
                    if (PauseStart.HasValue)
                    {
                        if (PauseStart >= _soir)
                            _heuresEffective = _soir.Subtract(TimeStart)
                                            + TimeSpan.FromTicks((((PauseStart.Value).Subtract(_soir)).Ticks / 100) * 125)
                                            + TimeSpan.FromTicks(((TimeEnd.Subtract((PauseEnd.Value)).Ticks / 100) * 125));
                        else if (PauseEnd >= _soir)
                            _heuresEffective = (PauseStart.Value).Subtract(TimeStart)
                                            + TimeSpan.FromTicks(((TimeEnd.Subtract((PauseEnd.Value)).Ticks / 100) * 125));
                        else
                                _heuresEffective = (PauseStart.Value).Subtract(TimeStart)
                                            + (_soir).Subtract(PauseEnd.Value)
                                            + TimeSpan.FromTicks(((TimeEnd.Subtract(_soir).Ticks / 100) * 125));
                    }
                    else
                    {
                        if (TimeStart >= _soir)
                            _heuresEffective = TimeSpan.FromTicks((TimeEnd.Subtract(TimeStart).Ticks / 100) * 125);
                        else
                            _heuresEffective = TimeSpan.FromTicks((TimeEnd.Subtract(_soir).Ticks / 100) * 25)
                                            + _soir.Subtract(TimeStart);
                    }
                }
                else
                    _heuresEffective = _totHeures;

                if (_isweekend)
                    _heuresEffective = TimeSpan.FromTicks((_heuresEffective.Ticks / 100) * 125);

                return _heuresEffective;
            }
        }
        public bool IsNew
        {
            get { return _isnew; }
            set { _isnew = value; }
        }
        //CONSTRUCTEUR
        public Work(Work w)
        {
            IdWork = w.IdWork;
            TimeStart = w.TimeStart;
            TimeEnd = w.TimeEnd;
            PauseStart = w.PauseStart;
            PauseEnd = w.PauseEnd;
            IdAlarm = w.IdAlarm;
            Alarm = w.Alarm;
            ListTasks = w.ListTasks;
            Description = w.Description;
            IsNew = w.IsNew;
            this._totHeures = w.TotHeures;
            this._heuresEffective = w.HeuresEffective;

        }
        //METHODES
        public static Work GetNewWork(Work tempW)
        {
            Work w = new Work(tempW) { IdWork = -1 };
            return w;
        }

    }
}
