﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifenoteData.Entity
{
    public partial class Note
    {
        //CONSTRUCTEUR
        public Note()
        { }
        public Note(Note _n)
        {
            this.Id = _n.Id;
            this.TimeStart = _n.TimeStart;
            this.TimeEnd = _n.TimeEnd;
            this.Description = _n.Description;
            this.IdListTask = _n.IdListTask;
            this.IdAlarm = _n.IdAlarm;
            this.Alarm = _n.Alarm;
            this.ListTask = _n.ListTask;
        }
        private bool _isnew = false;

        public bool IsNewNote
        {
            get { return _isnew; }
            set { _isnew = value; }
        }
        //METHODES
        public static Note GetNewNote(Note tempN)
        {
            Note n = new Note(tempN);
            n.Id = -1;
            return n;
        }

    }
}