﻿using LifenoteData.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifenoteWPF.Reporting
{
    public class WorkReporting
    {
        private List<InfoWork> _listInfoWork;

        public WorkReporting(List<InfoWork> _listIW)
        {
            _listInfoWork = _listIW;
        }
        public List<InfoWorkReporting> ListeInfoWorkReporting
        {
            get
            {
                List<InfoWorkReporting> list = new List<InfoWorkReporting>();
                foreach (InfoWork iw in _listInfoWork)
                {
                    list.Add(new InfoWorkReporting(iw));
                }
                return list;
            }
            set { }
        }

    }
    public class InfoWorkReporting
    {
        public InfoWorkReporting(InfoWork iw)
        {
            this.DateWork = iw.DateWork;
            this.StartWork = iw.StartWork;
            this.EndWork = iw.EndWork;
            this.IdWork = iw.IDwork;
            this.TotTime = iw.TotTime;
            this.RealTime = iw.RealTime;
        }
        public TimeSpan StartWork { get; set; }
        public TimeSpan EndWork { get; set; }
        public TimeSpan TotTime { get; set; }
        public TimeSpan RealTime { get; set; }
        public int IdWork { get; set; }
        public DateTime DateWork { get; set; }
    }
    
}
