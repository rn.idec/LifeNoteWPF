﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;
using MahApps.Metro;
using System.Windows.Media;
using LifenoteWPF.Helpers;

namespace LifenoteWPF
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
        }
        protected override void OnStartup(StartupEventArgs e)
        {
            // get the current app style (theme and accent) from the application
        Tuple<AppTheme, Accent> appStyle = ThemeManager.DetectAppStyle(Application.Current);

            base.OnStartup(e);

        }
    }
}
