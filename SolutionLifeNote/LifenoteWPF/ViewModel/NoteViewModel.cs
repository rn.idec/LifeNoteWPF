﻿using LifenoteData.Entity;
using LifenoteWPF.Helpers;
using LifenoteWPF.View;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace LifenoteWPF.ViewModel
{
    public class NoteViewModel : ViewModelBase
    {
        //CONSTRUCTEUR
        public NoteViewModel(Note n)
        {

            if (n.IsNewNote)
            {
                _currentNote = n;
                _currentListTask = n.ListTask;
            }
            else
            {
                _ListTasks = new ObservableCollection<ListTask>(ctx.ListTasks);
                foreach (ListTask lt in _ListTasks)
                {
                    foreach (Note no in lt.Notes)
                    {
                        if (no.Id == n.Id)
                        {
                            _currentNote = no;
                            _currentListTask = no.ListTask;
                            if (_currentNote.Alarm != null)
                                InfoTimeAlarm = _currentNote.Alarm.TimeAlarm;
                        }
                    }
                }
            }
            InfoDateTask = _currentListTask.DateOfList;
            _datecopy = _currentListTask.DateOfList;
            CopyListTask = ListTask.GetNewListTask(_datecopy);
            CopyNote = Note.GetNewNote(_currentNote);
        }
        //VARIABLES
        private Note _currentNote;
        private bool _IdLTisOk = false;
        bool abortsave = false;
        private DateTime _datecopy;
        private bool needcopy;
        private Note _copyNote;
        private ListTask _copyLT;
        bool foundAlarm = false;
        //PROPRIETES
        public Note CurrentNote
        {
            get
            {
                if (_currentNote == null)
                    _currentNote = new Note();
                return _currentNote;
            }
            set
            {
                _currentNote = value;

            }
        }
        public Note CopyNote
        {
            get
            {
                if (_copyNote == null)
                    _copyNote = new Note();
                return _copyNote;
            }
            set
            {
                _copyNote = value;

            }
        }
        public ListTask CopyListTask
        {
            get
            {
                if (_copyLT == null)
                    _copyLT = new ListTask();
                return _copyLT;
            }
            set
            {
                _copyLT = value;

            }
        }
        //COMMAND
        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(param => this.Save());
                }
                return _saveCommand;
            }
        }
        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(param => this.DeleteNote());
                }
                return _deleteCommand;
            }
        }
        public ICommand AnnulerCommand
        {
            get
            {
                if (_annulerCommand == null)
                {
                    _annulerCommand = new RelayCommand(param => this.Annuler());
                }
                return _annulerCommand;
            }
        }
        //METHODES
        public void Save()
        {
            //check if already another alarm existe
            if (InfoTimeAlarm != null) //check if alarm is empty if yes = delete
            {
                foreach (Alarm a in ctx.Alarms) //check if already another alarm existe
                {
                    if (InfoTimeAlarm == a.TimeAlarm)
                    {
                        _currentNote.IdAlarm = a.IdAlarm;
                        foundAlarm = true;
                    }
                }
                if (!foundAlarm)
                {
                    _currentNote.Alarm = new Alarm() { IsNewAlarm = true, TimeAlarm = new System.TimeSpan(0, 0, 0) };
                    _currentNote.Alarm.TimeAlarm = TimeSpan.Parse((InfoTimeAlarm.ToString()));
                }
            }
            if (_currentNote.IsNewNote)
            {
                foreach (ListTask lt in ctx.ListTasks)
                {
                    if (lt.DateOfList == InfoDateTask)
                    {
                        _currentListTask = lt;
                        _currentNote.IdListTask = _currentListTask.IdListTask;
                        _currentNote.ListTask = _currentListTask;
                        _IdLTisOk = true;
                    }
                }
                //add to contexte
                if (!_IdLTisOk)
                {
                    ctx.ListTasks.Add(_currentListTask);
                }
                _currentListTask.Notes.Add(_currentNote);
            }
            else
            {
                if (!_currentNote.IsNewNote && _datecopy != InfoDateTask)
                {// if yes ask if want to do a copy
                    MessageBoxResult result = MessageBox.Show("You have change the day of the Note, would you keep also the orignal Note?", "Alert!", MessageBoxButton.YesNoCancel);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            needcopy = true;
                            break;
                        case MessageBoxResult.No:
                            needcopy = false;
                            break;
                        case MessageBoxResult.Cancel:
                            abortsave = true;
                            break;
                    }
                    if (!abortsave)
                    {
                        _currentListTask.Notes.Remove(_currentNote);
                        //check if already listtask existe
                        foreach (ListTask lt in ctx.ListTasks)
                        {
                            if (lt.DateOfList == InfoDateTask)
                            {
                                _currentNote.IdListTask = lt.IdListTask;
                                _currentNote.ListTask = lt;
                                lt.Notes.Add(_currentNote);
                                _copyLT = lt;
                                _IdLTisOk = true;
                            }
                        }
                        if (!_IdLTisOk)
                        {
                            CopyListTask.DateOfList = InfoDateTask;
                            //check new info
                            _currentNote.IdListTask = _copyLT.IdListTask;
                            _currentNote.ListTask = _copyLT;
                            _copyLT.Notes.Add(_currentNote);
                        }
                        //now check the old listtask
                        if (needcopy)
                        {
                            _currentListTask.Notes.Add(_copyNote);
                        }
                        //now _currentListTask is read  for savechanges and note to add in ctx

                        if (!_IdLTisOk)
                            ctx.ListTasks.Add(_copyLT);
                    }
                }
            }

            if (!abortsave)
            {
                try
                {
                    if (!foundAlarm)
                        if (InfoTimeAlarm != null)
                            ctx.Alarms.Add(_currentNote.Alarm);
                    ctx.SaveChanges();
                    MyInfoUser.Content = "You Saved in the Database";
                    ChangeBackgroundInfo();
                }
                catch (Exception e)
                {
                    MessageBox.Show("{0}", e.ToString());
                }
                RefreshInfo();
            }
        }
        public void DeleteNote()
        {
            bool flag = false;
            MessageBoxResult result = MessageBox.Show("You really want to delete?", "Alert!", MessageBoxButton.YesNo);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    break;
                case MessageBoxResult.No:
                    flag = true;
                    break;
            }
            if (!flag)
            {
                if (_currentNote.IsNewNote)
                {
                    _currentNote = null;
                    _currentListTask = null;
                }
                else
                {
                    foreach (ListTask lt in ctx.ListTasks)
                    {
                        bool _find = false;
                        foreach (Note n in lt.Notes)
                        {
                            if (n.Id == _currentNote.Id)
                            {
                                lt.Notes.Remove(n);
                                _find = true;
                            }
                            if (_find)
                                break;
                        }
                    }
                    foreach (Note _n in ctx.Notes)
                    {
                        if (_n.Id == _currentNote.Id)
                        {
                            ctx.Notes.Remove(_n);
                        }
                    }
                    foreach (Alarm a in ctx.Alarms)
                    {
                        foreach (Note _n in a.Notes)
                        {
                            if (_n.Id == _currentNote.Id)
                                a.Notes.Remove(_n);
                        }
                    }
                }
                try
                {
                    ctx.SaveChanges();
                    RefreshInfo();
                    MyInfoUser.Content = "Deleted succesfully";
                    ChangeBackgroundInfo();
                }
                catch (Exception e)
                {
                    System.Windows.MessageBox.Show(e.ToString());
                }
            }
        }
        public void Annuler()
        {
            if (MessageBox.Show("Are you sure?", "Annuler", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                return;
            }
            else
            {
                RefreshInfo();
            }
        }
        public void RefreshInfo()
        {
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsNote.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackUCEvent.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).Refresh();
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabCalendar.IsSelected = true;
        }
    }
}
