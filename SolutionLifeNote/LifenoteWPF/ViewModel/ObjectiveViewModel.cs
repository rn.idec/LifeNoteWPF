﻿using LifenoteData.Entity;
using LifenoteWPF.Helpers;
using LifenoteWPF.View;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace LifenoteWPF.ViewModel
{
    public class ObjectiveViewModel : ViewModelBase
    {
        //CONSTRUCTEUR
        public ObjectiveViewModel(Objective ob)
        {
            if (ob.IsNewObjective)
            {
                _currentObjective = ob;
                _currentListTask = ob.ListTask;
            }
            else
            {
                _ListTasks = new ObservableCollection<ListTask>(ctx.ListTasks);
                foreach (ListTask lt in _ListTasks)
                {
                    foreach (Objective obj in lt.Objectives)
                    {
                        if (obj.IdObjective == ob.IdObjective)
                        {
                            _currentObjective = obj;
                            _currentListTask = obj.ListTask;
                            _findObjective = true;
                        }
                    }
                }
                if (!_findObjective)
                    _currentObjective = new Objective() { IsNewObjective = true, LimitDay = DateTime.Today };
            }
            InfoDateTask = _currentListTask.DateOfList;
            _datecopy = _currentListTask.DateOfList;
            _copyLT = ListTask.GetNewListTask(_datecopy);
        }
        //VARIABLES
        private Objective _currentObjective;
        private DateTime _datecopy;
        private bool _IdLTisOk;
        private bool _findObjective;
        private ListTask _copyLT;
        //PROPRIETES
        public Objective CurrentObjective
        {
            get { return _currentObjective; }
            set
            {
                _currentObjective = value;
                FirePropertyChanged();
            }
        }
        //COMMAND
        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(param => this.Save());
                }
                return _saveCommand;
            }
        }
        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(param => this.DeleteObjective());
                }
                return _deleteCommand;
            }
        }
        public ICommand AnnulerCommand
        {
            get
            {
                if (_annulerCommand == null)
                {
                    _annulerCommand = new RelayCommand(param => this.Annuler());
                }
                return _annulerCommand;
            }
        }
        //METHODES
        public void Save()
        {
            if (_currentObjective.LimitDay < InfoDateTask)
            {
                MyInfoUser.Content = "Please change the period";
                ChangeBackgroundInfo();
            }
            else
            {
                if (_currentObjective.IsNewObjective)
                {
                    _currentObjective = Objective.GetNewObjective(_currentObjective);
                    foreach (ListTask lt in ctx.ListTasks)
                    {
                        if (lt.DateOfList == InfoDateTask)
                        {
                            _currentListTask = lt;
                            _currentObjective.IdListTask = lt.IdListTask;
                            _currentObjective.ListTask = lt;
                            _IdLTisOk = true;
                        }
                    }
                    if (!_IdLTisOk)
                    {
                        _currentListTask.DateOfList = InfoDateTask;
                        ctx.ListTasks.Add(_currentListTask);
                    }
                    ctx.Objectives.Add(_currentObjective);
                }
                else
                { //check if date was changed
                    if (!_currentObjective.IsNewObjective && _datecopy != InfoDateTask)
                    {
                        _currentListTask.Objectives.Remove(_currentObjective);
                        //check if already listtask existe
                        foreach (ListTask lt in ctx.ListTasks)
                        {
                            if (lt.DateOfList == InfoDateTask)
                            {
                                _currentObjective.IdListTask = lt.IdListTask;
                                _currentObjective.ListTask = lt;
                                lt.Objectives.Add(_currentObjective);
                                _copyLT = lt;
                                _IdLTisOk = true;
                            }
                        }
                        if (!_IdLTisOk)
                        {
                            _copyLT.DateOfList = InfoDateTask;
                            //check new info
                            _currentObjective.IdListTask = _copyLT.IdListTask;
                            _currentObjective.ListTask = _copyLT;
                            _copyLT.Objectives.Add(_currentObjective);
                            ctx.ListTasks.Add(_copyLT);
                        }
                    }
                }
                try
                {
                    ctx.SaveChanges();
                    MyInfoUser.Content = "You Saved in the Database";
                    ChangeBackgroundInfo();
                }
                catch (Exception e)
                {

                    MessageBox.Show("{0}", e.ToString());

                }
                RefreshInfo();
            }
        }
        public void DeleteObjective()
        {
            bool flag = false;
            MessageBoxResult result = MessageBox.Show("You really want to delete?", "Alert!", MessageBoxButton.YesNo);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    break;
                case MessageBoxResult.No:
                    flag = true;
                    break;
            }
            if (!flag)
            {
                if (_currentObjective.IsNewObjective)
                {
                    _currentObjective = null;
                    _currentListTask = null;
                }
                else
                {
                    foreach (ListTask lt in ctx.ListTasks)
                    {
                        bool _find = false;
                        foreach (Objective obje in lt.Objectives)
                        {
                            if (obje.IdObjective == _currentObjective.IdObjective)
                            {
                                lt.Objectives.Remove(obje);
                                _find = true;
                            }
                            if (_find)
                                break;
                        }
                    }
                    foreach (Objective _obj in ctx.Objectives)
                    {
                        if (_obj.IdObjective == _currentObjective.IdObjective)
                        {
                            ctx.Objectives.Remove(_obj);
                        }
                    }

                }
                try
                {
                    ctx.SaveChanges();
                    RefreshInfo();
                    MyInfoUser.Content = "Deleted succesfully";
                    ChangeBackgroundInfo();
                }
                catch (Exception e)
                {
                    System.Windows.MessageBox.Show(e.ToString());
                }
            }
        }
        public void Annuler()
        {
            if (MessageBox.Show("Are you sure?", "Annuler", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                return;
            }
            else
            {
                RefreshInfo();
            }
        }
        public void RefreshInfo()
        {
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsOBJ.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackUCEvent.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).Refresh();
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabCalendar.IsSelected = true;
        }
    }
}
