﻿using LifenoteData.Entity;
using LifenoteWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LifenoteWPF.View
{
    /// <summary>
    /// Logique d'interaction pour UcListNotes.xaml
    /// </summary>
    public partial class UcListNotes : UserControl
    {
        public UcListNotes()
        {
            InitializeComponent();
        }
        private void Selected_Note(object sender, System.Windows.RoutedEventArgs e)
        {
            
            Note n = null;

            if (madatagridNOTE.SelectedItem == null) return;

            n = (madatagridNOTE.SelectedItem as Note);

            UserControlNote ucn = new UserControlNote(n);

            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsNote.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsNote.Children.Add(ucn);
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabN.IsSelected = true;
        }
    }
}
