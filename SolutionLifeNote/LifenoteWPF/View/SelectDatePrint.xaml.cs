﻿using LifenoteWPF.Reporting;
using System;
using System.Collections.Generic;
using System.Windows;
using MahApps.Metro.Controls;

namespace LifenoteWPF.View
{
    /// <summary>
    /// Logique d'interaction pour SelectDatePrint.xaml
    /// </summary>
    public partial class SelectDatePrint : MetroWindow
    {
        public SelectDatePrint(List<InfoWork> _listIW)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DataContext = this;
            _listinfowork = _listIW;
            DateTime _today = DateTime.Today;
            this.DateDebutPrint = new DateTime(_today.Year,_today.Month,1);
            this.DateFinPrint = new DateTime(_today.Year, _today.Month, (DateTime.DaysInMonth(_today.Year, _today.Month)));
        }
        private DateTime _dateDebutPrint;
        private DateTime _dateFinPrint;
        private List<InfoWork> _listinfowork;
        public List<InfoWork> Listinfowork
        {
            get { return _listinfowork; }
            set { _listinfowork = value; }
        }
        public DateTime DateDebutPrint
        {
            get { return _dateDebutPrint; }
            set { _dateDebutPrint = value; }
        }
        public DateTime DateFinPrint
        {
            get { return _dateFinPrint; }
            set { _dateFinPrint = value; }
        }

        private void Ok_Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime dbP = DateDebutPrint;
            DateTime dFP = DateFinPrint;
            List<InfoWork> templist = new List<InfoWork>();
            foreach (InfoWork iw in Listinfowork)
            {
                if (iw.DateWork >= dbP && iw.DateWork <= dFP)
                    templist.Add(iw);
            }
            FormReport report = new FormReport(new WorkReporting(templist));
            report.Show();
            var targetWindow = Window.GetWindow(this);
            targetWindow.Close();
        }
    }
}
