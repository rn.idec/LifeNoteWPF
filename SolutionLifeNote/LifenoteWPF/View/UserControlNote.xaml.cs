﻿using LifenoteData.Entity;
using LifenoteWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LifenoteWPF.View
{
    /// <summary>
    /// Logique d'interaction pour UserControlNote.xaml
    /// </summary>
    public partial class UserControlNote : UserControl
    {
        public UserControlNote()
        {
            InitializeComponent();
        }
        public UserControlNote(Note n) : this()
        {
            DataContext = new NoteViewModel(n);
        }
        private string timeEndOldSender = "00:00";
        private string timeStartOldSender = "00:00";
        private string timeAlarmOldSender = "00:00";
        private bool CheckTime(string TestThisTime)
        {
            int htime = 0;
            int mtime = 0;
            string[] TimeSplitted = TestThisTime.Split(':');
            bool okParseH = int.TryParse(TimeSplitted[0], out htime);
            bool okParseM = int.TryParse(TimeSplitted[1], out mtime);
            if (okParseH && okParseM)
            {
                if (htime > 23 || mtime > 59)
                    okParseH = false;
            }
            else
                okParseH = false;
            return okParseH;
            
        }
        private void txtALARM_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (TimeAlarm.Text != "")
            {
                if (CheckTime(TimeAlarm.Text))
                    timeAlarmOldSender = textBox.Text;
                else
                    textBox.Text = timeAlarmOldSender;
            }
        }

        private void txtTimeStart_TextChanged(object sender, TextChangedEventArgs e)
        {

            TextBox textBox = sender as TextBox;

            if (CheckTime(txtTimeStart.Text))
                timeStartOldSender = textBox.Text;
            else
                textBox.Text = timeStartOldSender;
        }

        private void txtTimeEnd_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;

            if (CheckTime(txtTimeEnd.Text))
                timeEndOldSender = textBox.Text;
            else
                textBox.Text = timeEndOldSender;
        }

        private void Time_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox.Text == "")
            {
                textBox.Text = "00:00";
            }
        }
    }
}
