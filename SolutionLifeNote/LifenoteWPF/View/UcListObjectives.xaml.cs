﻿using LifenoteData.Entity;
using LifenoteWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LifenoteWPF.View
{
    /// <summary>
    /// Logique d'interaction pour UcListObjectives.xaml
    /// </summary>
    public partial class UcListObjectives : UserControl
    {
        public UcListObjectives()
        {
            InitializeComponent();
        }
        private void Selected_Objective(object sender, System.Windows.RoutedEventArgs e)
        {
            EventViewModel evm = ((EventViewModel)DataContext);

            Objective ob = null;

            if (madatagridOBJ.SelectedItem == null) return;
            ob = (madatagridOBJ.SelectedItem as Objective);


            UserControlObjective ucn = new UserControlObjective(ob);

            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsOBJ.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsOBJ.Children.Add(ucn);
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabO.IsSelected = true;
        }
    }
}
