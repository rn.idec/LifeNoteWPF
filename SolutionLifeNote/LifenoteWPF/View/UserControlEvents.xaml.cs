﻿using System;
using System.Windows.Input;
using System.Windows.Controls;
using LifenoteWPF.ViewModel;
using System.Windows;
using LifenoteWPF.View;
using LifenoteData.Entity;
using System.Windows.Controls.Primitives;
using System.Collections.ObjectModel;

namespace LifenoteWPF.View
{
    /// <summary>
    /// Logique d'interaction pour UserControlEvent.xaml
    /// </summary>
    public partial class UserControlEvent : UserControl
    {
        public UserControlEvent()
        {
            InitializeComponent();
        }
        public UserControlEvent(DateTime dayselected) : this()
        {
            DataContext = new EventViewModel(dayselected);
        }
    }
}
