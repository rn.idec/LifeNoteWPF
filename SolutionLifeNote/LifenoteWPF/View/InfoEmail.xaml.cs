﻿using LifenoteData.Entity;
using LifenoteWPF.EmailXML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Xsl;
using MahApps.Metro.Controls;
using System.Security.Claims;
using LifenoteWPF.Helpers;

namespace LifenoteWPF.View
{
    /// <summary>
    /// Logique d'interaction pour InfoEmail.xaml
    /// </summary>
    public partial class InfoEmail : MetroWindow
    {
        //VARIABLES
        private DateTime _dateDebut;
        private DateTime _dateFin;
        private List<EmailWork> _listinfowork;
        private string _destinataire="rexnez@gmail.com";
        bool errorMail = false;
        static string xmlfile = "XMLFileStock.xml";     //le nom du fichier xml s'appel "XMLFileStock"
        List<string> templist = new List<string>();
        //CONSTUCTEUR
        public InfoEmail(List<EmailWork> _listIW)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DataContext = this;
            ListBaseWork = _listIW;
            DateTime _today = DateTime.Today;
            _listinfowork = new List<EmailWork>();
            this.DateDebut = new DateTime(_today.Year, _today.Month, 1);
            this.DateFin = new DateTime(_today.Year, _today.Month, (DateTime.DaysInMonth(_today.Year, _today.Month)));
            foreach (EmailWork EmailW in ListBaseWork)
            {
                if (EmailW.DateWork >= DateDebut && EmailW.DateWork <= DateFin)
                    _listinfowork.Add(EmailW);
            }
        }
        //METHODES
        private void Ok_Button_Click(object sender, RoutedEventArgs e)
        {
            templist = new List<string>();
            //data in a list of string
            foreach (EmailWork iw in Listinfowork)
            {
                if (iw.DateWork >= DateDebut && iw.DateWork <= DateFin)
                {
                    templist.Add(iw.DateWork.ToString("ddd dd.MM.yyyy"));
                    templist.Add(iw.StartWork.ToString(@"hh\:mm"));
                    templist.Add(iw.EndWork.ToString(@"hh\:mm"));
                    if (iw.PauseStart == null)
                        templist.Add("n/a");
                    else
                        templist.Add((iw.PauseStart != null ? iw.PauseStart.Value.ToString(@"hh\:mm") : "n/a"));
                    if (iw.PauseEnd == null)
                        templist.Add("n/a");
                    else
                        templist.Add((iw.PauseEnd != null ? iw.PauseEnd.Value.ToString(@"hh\:mm") : "n/a"));
                }
            }
            //get info XML
            XmlDocument doc = new XmlDocument();
            doc = CreateXmlDocument(templist);
            doc.Save(xmlfile);

            //convert xml to string for the text, its only for check data in XML
            StringWriter sw = new StringWriter();
            XmlTextWriter tx = new XmlTextWriter(sw);
            doc.WriteTo(tx);
            string str = sw.ToString();
            //some change data
            string xsltInfo = CreateXsltString();
            string resultHTML = TransformXMLToHTML(str, xsltInfo);
            #region EMAIL
            //envoie d'email
            if (!errorMail)
            {
                try
                {
                    MailMessage mail = new MailMessage("rexnez@gmail.com", _destinataire);
                    mail.Subject = "WORK RAPPORT BY XML";

                    mail.Body = "Enjoy your work";
                    // Add HTML to MemoryStream                                    
                    MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(resultHTML));
                    // Content Type
                    ContentType contentType = new ContentType("text/html");
                    // Attachment
                    Attachment addToEmail = new Attachment(memoryStream, contentType);
                    addToEmail.NameEncoding = UTF8Encoding.UTF8;
                    addToEmail.TransferEncoding = TransferEncoding.Base64;
                    addToEmail.ContentDisposition.DispositionType = DispositionTypeNames.Attachment;

                    mail.Attachments.Add(addToEmail);
                    //info serveur smtp
                    SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                    smtpClient.Credentials = new System.Net.NetworkCredential()
                    {
                        #region *******
                        UserName = "ûsername here",
                        Password = "password here",
                        #endregion
                    };
                    smtpClient.EnableSsl = true;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                            System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                            System.Security.Cryptography.X509Certificates.X509Chain chain,
                            System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };

                    smtpClient.Send(mail);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    if (ex == null)
                        errorMail = true;
                }
                finally
                {
                    if (errorMail)
                        MessageBox.Show("Email SUCCESSFULLY sent");
                    else
                        MessageBox.Show("FAILED to send email!");
                }
            }
            #endregion
            var targetWindow = Window.GetWindow(this);
            targetWindow.Close();

        }
        static XmlDocument CreateXmlDocument(List<string> _templist)
        {
            XmlDocument _doc = new XmlDocument();
            XmlDeclaration declarationXml = _doc.CreateXmlDeclaration("1.0", "utf-8", null);
            _doc.AppendChild(declarationXml);
            //WORKS
            XmlElement racine = _doc.CreateElement("Works");
            _doc.AppendChild(racine);
            //FOREACH WORK
            for (int i = 0; i < _templist.Count; i++)
            {
                //WORK
                XmlElement tempWork = _doc.CreateElement("Work");
                racine.AppendChild(tempWork);
                //DATE
                XmlElement tempDate = _doc.CreateElement("Date");
                XmlText txtDate = _doc.CreateTextNode(_templist[i]);
                tempDate.AppendChild(txtDate);
                tempWork.AppendChild(tempDate);
                i++;
                //TIME START
                XmlElement tempsTimeStart = _doc.CreateElement("TimeStart");
                XmlText txtTS = _doc.CreateTextNode(_templist[i]);
                tempsTimeStart.AppendChild(txtTS);
                tempWork.AppendChild(tempsTimeStart);
                i++;
                //TIME END
                XmlElement tempsTimeEnd = _doc.CreateElement("TimeEnd");
                XmlText txtTE = _doc.CreateTextNode(_templist[i]);
                tempsTimeEnd.AppendChild(txtTE);
                tempWork.AppendChild(tempsTimeEnd);
                i++;
                //PÂUSE START
                XmlElement tempPauseStart = _doc.CreateElement("PauseStart");
                XmlText txtPS = _doc.CreateTextNode(_templist[i]);
                tempPauseStart.AppendChild(txtPS);
                tempWork.AppendChild(tempPauseStart);
                i++;
                //PAUSE END
                XmlElement tempPauseEnd = _doc.CreateElement("PauseEnd");
                XmlText txtPE = _doc.CreateTextNode(_templist[i]);
                tempPauseEnd.AppendChild(txtPE);
                tempWork.AppendChild(tempPauseEnd);
            }
            return _doc;
        }
        static string CreateXsltString()
        {
            string _myXslt = @"<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version = '1.0' xmlns:xsl = 'http://www.w3.org/1999/XSL/Transform'>
  <xsl:output method='html' omit-xml-declaration='yes'/>
  <xsl:template match = '/'>
    <html>
      <body>
        <h2> Work Rapport </h2>
        <table border = '1'>
          <tr bgcolor = '#9acd32'>
            <th style = 'text-align:left' > Date </th>
            <th style = 'text-align:left' > TimeStart </th>
            <th style = 'text-align:left' > TimeEnd </th>
            <th style = 'text-align:left' > PauseStart </th>
            <th style = 'text-align:left' > PauseEnd </th>
          </tr>
          <xsl:for-each select = 'Works/Work'>
            <tr>
              <td>
                <xsl:value-of select = 'Date'/>
              </td>
              <td>
                <xsl:value-of select = 'TimeStart'/>
              </td>
              <td>
                <xsl:value-of select = 'TimeEnd'/>
              </td>
              <td>
                <xsl:value-of select = 'PauseStart'/>
              </td>
              <td>
                <xsl:value-of select = 'PauseEnd'/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>";
            return _myXslt;
        }
        public static string TransformXMLToHTML(string inputXml, string xsltString)
        {
            XslCompiledTransform transform = new XslCompiledTransform();
            using (XmlReader reader = XmlReader.Create(new StringReader(xsltString)))
            {
                transform.Load(reader);
            }
            StringWriter results = new StringWriter();
            using (XmlReader reader = XmlReader.Create(new StringReader(inputXml)))
            {
                transform.Transform(reader, null, results);
            }
            return results.ToString();
        }
        //PROPRIETES
        public List<EmailWork> Listinfowork
        {
            get { return _listinfowork; }
            set { _listinfowork = value; }
        }
        public List<EmailWork> ListBaseWork { get; set; }
        public DateTime DateDebut
        {
            get { return _dateDebut; }
            set { _dateDebut = value; }
        }
        public DateTime DateFin
        {
            get { return _dateFin; }
            set { _dateFin = value; }
        }
        public String Destinataire
        {
            get { return _destinataire; }
            set { value = _destinataire; }
        }
        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            Listinfowork = new List<EmailWork>();
            DataToSend.ItemsSource = null;

            foreach (EmailWork EmailW in ListBaseWork)
            {
                if (EmailW.DateWork >= DateDebut && EmailW.DateWork <= DateFin)
                    Listinfowork.Add(EmailW);
            }
            DataToSend.ItemsSource = Listinfowork;
        }
    }
}
